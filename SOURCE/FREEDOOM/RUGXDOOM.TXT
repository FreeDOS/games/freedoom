This is a FDNPKG .ZIP package for FreeDOS 1.1 made by me, Rugxulo.

It replaces the much older FD 1.0 package [2006] (which had Doom Legacy
1.5 + FreeDoom 0.4, I think ... but not recommended due to bugs [bridge!]
and copyright clashes, blech).

This contains a GPL'd Doom engine (SMMU) and FreeDoom (BSD'd) .WAD file
for a completely new but free/libre experience.

SMMU is "Smack My Marine Up" by Simon Howard, 3.30 circa 2000, compiled
by DJGPP v2 + Allegro 3.12. It is a derivative of MBF, which derives
from Boom, which came from the first (re)port of DOS Doom from the
Linux sources [sic].

FreeDoom's doom2.wad (28 MB!) is 0.8-beta1 circa 2012 (and requires
Boom or similar bugfixed + extended Doom engine).

Due to a small bug (eh?), SMMU by default doesn't work on my 6 GB machine
in pure FreeDOS. CWSDPMI r7 reports 4 GB free, but DJGPP apps usually
don't handle that very well. At least, I'm not sure the libc can handle
over 2 GB. Anyways, the sloppy fix is to just hardcode something like
32000000UL or 64000000UL in Z_ZONE.c instead of calling
_go32_dpmi_remaining_physical_memory(). Then it runs in pure DOS (though
without sound on this silly Intel HDA card, sadly. Maybe somebody will
make latest GPL'd DJGPP DOS Judas Player lib work here?).

http://www.piotrkn22.republika.pl/judas/index.html

It still may have bugs, e.g. "smmu -nosound" is the only way to
currently run under DOSEMU 1.4.0.0, but since you probably don't care
(and can use native OS build), I guess it's no huge loss. Also works
fine for me under DOSBox 0.73, though. Won't run under WinXP or similar
due to near ptrs, so the only port that I know of that works there
(320x200 only) is CDoom, which is similarly based upon Eternity
(the successor to SMMU, which I would've preferred here except it is
practically impossible to rebuild 3.31 beta7 [2004] for DJGPPv2 since it
lacks a makefile and is unmaintained, and even older 3.29's compile
doesn't work correctly either, doh!).

http://eternity.mancubus.net/ee-old/
http://www.gamers.org/pub/idgames/source/

Long story short: I did successfully recompile this (only) with "classic"
GCC 2.95.3 + Allegro 3.12, and with the Z_ZONE.c fix and removing
-DTCPIP from the makefile, it seems to run correctly with CWSDPMI r7
on my big RAM (6 GB) machine in pure FreeDOS. But YMMV, so feel free to
try whatever else you can think of. There is no support by anyone, and
almost nobody in the Doom world is interested in DOS (sadly).

P.S. It seems that Eric's IDLEDPMS TSR (from FDAPM package) gets
overriden by Doom's keyboard handler, so once it blanks, you're kinda
screwed!  :-(   Sorry, you'll have to disable it (or reset it at cmdline
to 59 minutes, max timeout).

rugxulo _AT_ gmail _DOT_ com
freedos-users@lists.sourceforge.net   (or something like that)

http://sites.google.com/site/rugxulo
http://www.sourceforge.net/projects/freedos
http://www.bttr-software.de/forum/

http://www.soulsphere.org/projects/smmu/
http://www.nongnu.org/freedoom/

http://homer.rice.edu/~sandmann/cwsdpmi/index.html
http://www.delorie.com/djgpp/getting.html
http://na.mirror.garr.it/mirrors/djgpp/

<EOF>
